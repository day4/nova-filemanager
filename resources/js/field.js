Nova.booting((Vue, router, store) => {
    Vue.component('index-filemanager', require('./components/IndexField'))
    Vue.component('detail-filemanager', require('./components/DetailField'))
    Vue.component('form-filemanager', require('./components/FormField'))
})
