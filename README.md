## Usage

**Requires** unisharp/laravel-filemanager: https://unisharp.github.io/laravel-filemanager/installation

```
use Day4\Filemanager\Filemanager;
    ...
    public function fields(Request $request)
        {
            return [
                ...
                Filemanager::make( __('File'), 'file')
                    ->rules('required')
            ]
        }

```