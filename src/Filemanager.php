<?php

namespace Day4\Filemanager;

use Laravel\Nova\Fields\Field;

class Filemanager extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'filemanager';

    /**
     * Set the type of files that may be selected.
     *
     * @param  array  $type
     * @return $this
     */
    public function type(array $type)
    {
        return $this->withMeta(['type' => $type]);
    }
}
